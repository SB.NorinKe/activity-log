package com.example.activitylog.dto;

import lombok.Data;

import java.util.UUID;


@Data
public class UpdateSupplierInfoRequestDto {
    private UUID supplierId;
    private String phone;
    private String address;
    private String accountNo;
}
