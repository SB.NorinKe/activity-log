package com.example.activitylog.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@Table(name = "ACTIVITY_LOG_VIEW")
@Entity
public class ActivityLog {

    @Id
    private UUID id;
    private String tableRefId;
    private String tableName;
    private String action;
    private String createdAt;
    private String name;
    private String oldName;
    private String email;
    private String oldEmail;
    private String address;
    private String oldAddress;
    private String credit;
    private String oldCredit;
    private String phone;
    private String oldPhone;
    private String accountNo;
    private String oldAccountNo;
    private String status;
    private String oldStatus;
}
